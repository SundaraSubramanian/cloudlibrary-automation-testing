package cloudlibraryAutomation;

import java.awt.AWTException;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class LogintotheApplication extends LaunchtheApplication {
	@Test			(priority=2)	//*(*(*(*(Enter Credentials and press Login)*)*)*)*\\
	public void login() throws Exception{
		System.out.println("Log into the Application using the User ID");
		setAuthorInfoForReports();
		//*(*(*(*(Enter the Library patron ID)*)*)*)*\\
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/textUserName")));
		WebElement Enter_UID= driver.findElement(By.id("com.txtr.android.mmm:id/textUserName"));
		Enter_UID.click();
		Enter_UID.sendKeys(User_Name);
		
		try{
					//*(*(*(*(Enter the Password)*)*)*)*\\
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/textUserPwd")));
			WebElement Enter_Pwd= driver.findElement(By.id("com.txtr.android.mmm:id/textUserPwd"));
			Enter_Pwd.click();
			Enter_Pwd.sendKeys("1111");
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/next_button")));
			WebElement LogintoLibrary= driver.findElement(By.id("com.txtr.android.mmm:id/next_button"));
			LogintoLibrary.click();
		} catch (TimeoutException  e) {
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/next_button")));
		WebElement LogintoLibrary= driver.findElement(By.id("com.txtr.android.mmm:id/next_button"));
		LogintoLibrary.click();
		}
		System.out.println("Login Successful - The Application now loads the Cloud Library Application ");

	}
	public void setAuthorInfoForReports() {
        ATUReports.setAuthorInfo("Sundara Subramanian =", Utils.getCurrentTime(),"1.0");
 }
 public void setIndexPageDescription() {
        ATUReports.indexPageDescription = "Bibliotheca Cloud Library Client Apps --> Android Application Testing";
 }
	@SuppressWarnings("deprecation")
	public void testME() {
        setAuthorInfoForReports();
        ATUReports.add("Step Desc", false);
        ATUReports.add("Step Desc", "inputValue", false);
        ATUReports.add("Step Desc", "expectedValue", "actualValue", false);
        ATUReports.add("Step Desc", "inputValue", "expectedValue","actualValue", false);
 }
	 public void testNewLogs() throws AWTException, IOException {
        ATUReports.add("INfo Step", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
        ATUReports.add("Pass Step", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
        WebElement element = driver.findElement(By.xpath("/html/body/div/h1/a"));
        ATUReports.add("Warning Step", LogAs.WARNING,new CaptureScreen(element));
        ATUReports.add("Fail step", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
 }
}
