package cloudlibraryAutomation;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class NavigatetoMybooksandReturnBooks extends ReadfromMyBooks {

	@Test (priority=7)
	public void Return_from_MyBooks() throws Exception{
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("android:id/action_bar_title")));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.ImageView[@index='0']")));
		//WebElement left_nav = driver.findElement(By.xpath("android:id/action_bar_title"));
		WebElement left_nav = driver.findElement(By.xpath("//android.widget.ImageView[@index='0']"));
		left_nav.click();
		//System.out.println("I am in Navigate to My Books and Retu  rn books ");
		System.out.println("Trying to click My books");
		//driver.navigate().back();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/leftnav_mybooks")));
		WebElement mybooks_title= driver.findElement(By.id("com.txtr.android.mmm:id/leftnav_mybooks"));
		mybooks_title.click();
			try{
				
				System.out.println("I am in My books");
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/mybookstwo_currentreading_iv")));
			WebElement return_books_button= driver.findElement(By.id("com.txtr.android.mmm:id/mybooks_listview_button"));
			return_books_button.click();
			WebElement View_Book_detail = driver.findElement(By.id("com.txtr.android.mmm:id/documentThumbnail"));
			View_Book_detail.click();
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/document_details_button_two")));
			WebElement Return_confirmation= driver.findElement(By.id("com.txtr.android.mmm:id/document_details_button_two"));
			Return_confirmation.click();
			wait.until(ExpectedConditions.presenceOfElementLocated(By.className("android.widget.Button")));
			WebElement ReturnBook = driver.findElement(By.xpath("//android.widget.Button[@text='Yes']"));
			ReturnBook.click();
			System.out.println("Book returned");
			wait.until(ExpectedConditions.presenceOfElementLocated(By.className("android.widget.Button")));
			WebElement SkipVote = driver.findElement(By.xpath("//android.widget.Button[@text='Close']"));
			SkipVote.click();
			
		} catch (TimeoutException  e) {
			System.out.println("No Books to Read");
			driver.navigate().back();
							}

	}
	public void setAuthorInfoForReports() {
        ATUReports.setAuthorInfo("Sundara Subramanian =", Utils.getCurrentTime(),"1.0");
 }
	
	 public void setIndexPageDescription() {
        ATUReports.indexPageDescription = "Bibliotheca Cloud Library Client Apps --> Android Application Testing";
 }
	
	@SuppressWarnings("deprecation")
	public void testME() {
        setAuthorInfoForReports();
        ATUReports.add("Step Desc", false);
        ATUReports.add("Step Desc", "inputValue", false);
        ATUReports.add("Step Desc", "expectedValue", "actualValue", false);
        ATUReports.add("Step Desc", "inputValue", "expectedValue","actualValue", false);
 }
	 	 public void testNewLogs() throws AWTException, IOException {
        ATUReports.add("INfo Step", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
        ATUReports.add("Pass Step", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
        WebElement element = driver.findElement(By.xpath("/html/body/div/h1/a"));
        ATUReports.add("Warning Step", LogAs.WARNING,new CaptureScreen(element));
        ATUReports.add("Fail step", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
 }
}
