package cloudlibraryAutomation;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class InitialLeftNavigation extends LogintotheApplication {

	@Test 		(priority=3)
	public void Initial_Left_Navigation() throws Exception{
		
		System.out.println("Asserting the Initial Navigation options");
				
		//*(*(*(*(Validate the Features button)*)*)*)*\\
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/leftnav_featured")));
		WebElement featured_title= driver.findElement(By.id("com.txtr.android.mmm:id/leftnav_featured"));
		assert featured_title.getText().equals("Featured");
		
		//*(*(*(*(Validate the Browse button)*)*)*)*\\
		WebElement browse_title= driver.findElement(By.id("com.txtr.android.mmm:id/leftnav_browse"));
		assert browse_title.getText().equals("Browse");
		
		//*(*(*(*(Validate the My Books button)*)*)*)*\\
		WebElement Mybooks_title= driver.findElement(By.id("com.txtr.android.mmm:id/leftnav_mybooks"));
		assert Mybooks_title.getText().equals("My Books");
		
		//*(*(*(*(Validate the Filters button)*)*)*)*\\
		WebElement Filters_title= driver.findElement(By.id("com.txtr.android.mmm:id/leftnav_filters"));
		assert Filters_title.getText().equals("Filters");
		
		//*(*(*(*(Validate the Settings button)*)*)*)*\\
		WebElement settings_title= driver.findElement(By.id("com.txtr.android.mmm:id/leftnav_settings"));
		assert settings_title.getText().equals("Settings");
		
		//*(*(*(*(Validate the Foot title button)*)*)*)*\\
		//WebElement libname_title= driver.findElement(By.id("com.txtr.android.mmm:id/leftnav_library_name"));
		//assert libname_title.getText().equals(Library_Name);
		
	}
	 public void setAuthorInfoForReports() {
         ATUReports.setAuthorInfo("Sundara Subramanian =", Utils.getCurrentTime(),"1.0");
  }
	
	 public void setIndexPageDescription() {
         ATUReports.indexPageDescription = "Bibliotheca Cloud Library Client Apps --> Android Application Testing";
  }
	@SuppressWarnings("deprecation")
	public void testME() {
        setAuthorInfoForReports();
        ATUReports.add("Step Desc", false);
        ATUReports.add("Step Desc", "inputValue", false);
        ATUReports.add("Step Desc", "expectedValue", "actualValue", false);
        ATUReports.add("Step Desc", "inputValue", "expectedValue","actualValue", false);
 }
	 
	 public void testNewLogs() throws AWTException, IOException {

        ATUReports.add("INfo Step", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
        ATUReports.add("Pass Step", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
        WebElement element = driver.findElement(By.xpath("/html/body/div/h1/a"));
        ATUReports.add("Warning Step", LogAs.WARNING,new CaptureScreen(element));
        ATUReports.add("Fail step", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
 }

}
