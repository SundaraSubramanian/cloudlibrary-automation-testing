package cloudlibraryAutomation;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class CheckoutBookFromFeaturedShelf  extends FeaturedShelves {
	
@Test (priority=5)
	public void Checkout_Book_From_Featured_Shelf() throws Exception {
		
	System.out.println("Navigation to Featured Shelf - Successful");
	
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/presentation_row_title_tv")));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/presentation_book_iv")));
		WebElement View_Book_detail = driver.findElement(By.id("com.txtr.android.mmm:id/presentation_book_iv"));
		View_Book_detail.click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/document_details_button_two")));
		try{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/document_details_button_two")));
			WebElement Book_checkout_options = driver.findElement(By.id("com.txtr.android.mmm:id/document_details_button_two"));
			//System.out.println(Book_checkout_options.getText());
			String Book_CheckOut_Options = Book_checkout_options.getText();
			System.out.println(Book_CheckOut_Options);
			if(Book_CheckOut_Options.equals("Borrow"))
			{
						Book_checkout_options.click();
						System.out.println("Book Checkout - Successful");
			try
			{
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/imageViewBook")));
				System.out.println("Checkout of E-Book- Successful");
				driver.navigate().back();
			} catch (TimeoutException e)
			{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/audioPlayerCoverArtIV")));
			System.out.println("Checkout of Audio Book- Successful");
			driver.navigate().back();
			}
			
			}
			else if (Book_CheckOut_Options.equals("Return"))
			{
				//Moving on to second book for Checking out
				System.out.println("First Book not available for Check out");
				driver.navigate().back();
				System.out.println("Did i go back?");
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.RelativeLayout[@index='1']")));
				WebElement second_book_options = driver.findElement(By.xpath("//android.widget.RelativeLayout[@index='1']"));
				second_book_options.click();
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/document_details_button_two")));
				//WebElement Book_checkout_options = driver.findElement(By.id("com.txtr.android.mmm:id/document_details_button_two"));
				//System.out.println(Book_checkout_options.getText());
				//String Book_CheckOut_Options = Book_checkout_options.getText();
				System.out.println(Book_CheckOut_Options);
				Book_checkout_options.click();
				System.out.println("2nd Book Checkout - Successful");
				
				
				/*wait.until(ExpectedConditions.presenceOfElementLocated(By.className("android.widget.Button")));
				WebElement ReturnBook = driver.findElement(By.xpath("//android.widget.Button[@text='Yes']"));
				ReturnBook.click();
					
				System.out.println("Book returned");
				
				wait.until(ExpectedConditions.presenceOfElementLocated(By.className("android.widget.Button")));
				WebElement SkipVote = driver.findElement(By.xpath("//android.widget.Button[@text='Close']"));
				SkipVote.click();
				driver.navigate().back();*/
			}
			else if (Book_CheckOut_Options.equals("Hold"))
			{
				System.out.println("First Book not available for Check out");
				driver.navigate().back();
				System.out.println("Did i go back?");
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.RelativeLayout[@index='1']")));
				WebElement second_book_options = driver.findElement(By.xpath("//android.widget.RelativeLayout[@index='1']"));
				second_book_options.click();
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/document_details_button_two")));
				//WebElement Book_checkout_options = driver.findElement(By.id("com.txtr.android.mmm:id/document_details_button_two"));
				//System.out.println(Book_checkout_options.getText());
				//String Book_CheckOut_Options = Book_checkout_options.getText();
				System.out.println(Book_CheckOut_Options);
				Book_checkout_options.click();
				System.out.println("2nd Book Checkout - Successful"); 
			}
			else
			{
				//System.out.println("I am in default else");
				driver.navigate().back();
			}
					
				} catch (TimeoutException e) {
					
					System.out.println("Trying to find the document one button");
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/document_details_button_one")));
					WebElement Read_book = driver.findElement(By.id("com.txtr.android.mmm:id/document_details_button_one"));
					String Read_book_Options = Read_book.getText();
					System.out.println(Read_book_Options);
					Read_book.click();
					wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/imageViewBook")));
					driver.navigate().back();
		}
		}
public void setAuthorInfoForReports() {
    ATUReports.setAuthorInfo("Sundara Subramanian =", Utils.getCurrentTime(),"1.0");
}

public void setIndexPageDescription() {
    ATUReports.indexPageDescription = "Bibliotheca Cloud Library Client Apps --> Android Application Testing";
}
@SuppressWarnings("deprecation")
public void testME() {
   setAuthorInfoForReports();
   ATUReports.add("Step Desc", false);
   ATUReports.add("Step Desc", "inputValue", false);
   ATUReports.add("Step Desc", "expectedValue", "actualValue", false);
   ATUReports.add("Step Desc", "inputValue", "expectedValue","actualValue", false);
}

public void testNewLogs() throws AWTException, IOException {

   ATUReports.add("INfo Step", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
   ATUReports.add("Pass Step", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
   WebElement element = driver.findElement(By.xpath("/html/body/div/h1/a"));
   ATUReports.add("Warning Step", LogAs.WARNING,new CaptureScreen(element));
   ATUReports.add("Fail step", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
}
}
