package cloudlibraryAutomation;
import java.awt.AWTException;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class ReadfromMyBooks extends CheckoutBookFromFeaturedShelf {
@Test (priority = 6)

public void Read_from_MyBooks() throws Exception{
		
	WebDriverWait wait = new WebDriverWait(driver, 80);
	wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.ImageView[@index='0']")));
	WebElement left_nav = driver.findElement(By.xpath("//android.widget.ImageView[@index='0']"));
	left_nav.click();
	wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/leftnav_mybooks")));
	WebElement mybooks_title= driver.findElement(By.id("com.txtr.android.mmm:id/leftnav_mybooks"));
	mybooks_title.click();
	try{
			wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/mybookstwo_currentreading_iv")));
			WebElement read_book= driver.findElement(By.id("com.txtr.android.mmm:id/mybookstwo_currentreading_iv"));
			read_book.click();
			try
				{
						wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/imageViewBook")));
						driver.swipe(435, 396, 112, 496, 900);
						driver.swipe(435, 396, 112, 496, 900);
						driver.swipe(435, 396, 112, 496, 900);
						driver.swipe(435, 396, 112, 496, 900);
						driver.swipe(435, 396, 112, 496, 900);
						driver.swipe(435, 396, 112, 496, 900);
						System.out.println("I have read the book");
						driver.navigate().back();
				} catch (TimeoutException e)
					{
						wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/audioPlayerCoverArtIV")));
						WebElement read_play_audio_book= driver.findElement(By.id("com.txtr.android.mmm:id/audioPlayerPlayPauseIB"));
						//read_play_audio_book.click();
						driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
						System.out.println("I have played the book");
						driver.manage().timeouts().implicitlyWait(240, TimeUnit.SECONDS);
						// read_play_audio_book.click();
						System.out.println("I have listened to the book");				
						//read_play_audio_book.click();
						driver.navigate().back();
					}
			
			} catch (TimeoutException e) {
						System.out.println("No Books to Read");
						System.out.println("Crossed - Navigate to My Books and read books");
						driver.navigate().back();
						
					}
	}

	public void setAuthorInfoForReports() {
        ATUReports.setAuthorInfo("Sundara Subramanian =", Utils.getCurrentTime(),"1.0");
 }
	
	 public void setIndexPageDescription() {
        ATUReports.indexPageDescription = "Bibliotheca Cloud Library Client Apps --> Android Application Testing";
 }
	
	
	@SuppressWarnings("deprecation")
	public void testME() {
        setAuthorInfoForReports();
        ATUReports.add("Step Desc", false);
        ATUReports.add("Step Desc", "inputValue", false);
        ATUReports.add("Step Desc", "expectedValue", "actualValue", false);
        ATUReports.add("Step Desc", "inputValue", "expectedValue","actualValue", false);
 }
	 
	 public void testNewLogs() throws AWTException, IOException {

        ATUReports.add("INfo Step", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
        ATUReports.add("Pass Step", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
        WebElement element = driver.findElement(By.xpath("/html/body/div/h1/a"));
        ATUReports.add("Warning Step", LogAs.WARNING,new CaptureScreen(element));
        ATUReports.add("Fail step", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
 }
}
