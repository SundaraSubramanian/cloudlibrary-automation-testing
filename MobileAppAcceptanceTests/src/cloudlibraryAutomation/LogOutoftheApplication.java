package cloudlibraryAutomation;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class LogOutoftheApplication extends BookOfInterest {

	@Test (priority = 9)	
	public void Logout() throws Exception 
	{
		
		
		WebDriverWait Wait5 = new WebDriverWait(driver, 80);
		Wait5.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.ImageView[@index='0']")));
		WebElement left_nav = driver.findElement(By.xpath("//android.widget.ImageView[@index='0']"));
		left_nav.click();
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/leftnav_settings")));
		WebElement settings_title= driver.findElement(By.id("com.txtr.android.mmm:id/leftnav_settings"));
		settings_title.click();
		
		WebDriverWait Wait2 = new WebDriverWait(driver, 80);
		Wait2.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='Email Notification']")));
		WebElement logout_button= driver.scrollTo("Log Out of Account");
		logout_button.click();
		
		
		WebDriverWait Wait3 = new WebDriverWait(driver, 80);
		Wait3.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[@index='1']")));
		WebElement confirm_logout = driver.findElement(By.xpath("//android.widget.Button[@index='1']"));
		confirm_logout.click();
		
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='"+Test_Environment+"']")));
		
		
	}
	public void setAuthorInfoForReports() {
        ATUReports.setAuthorInfo("Sundara Subramanian =", Utils.getCurrentTime(),"1.0");
 }
	
	 public void setIndexPageDescription() {
        ATUReports.indexPageDescription = "Bibliotheca Cloud Library Client Apps --> Android Application Testing";
 }
	
	@SuppressWarnings("deprecation")
	public void testME() {
        setAuthorInfoForReports();
        ATUReports.add("Step Desc", false);
        ATUReports.add("Step Desc", "inputValue", false);
        ATUReports.add("Step Desc", "expectedValue", "actualValue", false);
        ATUReports.add("Step Desc", "inputValue", "expectedValue","actualValue", false);
 }
	 
	 public void testNewLogs() throws AWTException, IOException {

        ATUReports.add("INfo Step", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
        ATUReports.add("Pass Step", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
        WebElement element = driver.findElement(By.xpath("/html/body/div/h1/a"));
        ATUReports.add("Warning Step", LogAs.WARNING,new CaptureScreen(element));
        ATUReports.add("Fail step", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
 }
	
}
