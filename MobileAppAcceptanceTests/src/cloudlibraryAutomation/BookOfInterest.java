package cloudlibraryAutomation;

import org.testng.annotations.Test;
import java.awt.AWTException;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class BookOfInterest extends NavigatetoMybooksandReturnBooks {
	
	@Test (priority = 5)
	public void booksofinterest() throws Exception{
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.ImageView[@index='0']")));
		WebElement left_nav = driver.findElement(By.xpath("//android.widget.ImageView[@index='0']"));
		left_nav.click();
		
		//Clicking the Book of Interest in Featured shelf
		WebElement featured_title= driver.findElement(By.id("com.txtr.android.mmm:id/leftnav_featured"));
		assert featured_title.getText().equals("Featured");
		featured_title.click();
		
		//Using the First book as a book of interest in Featured shelf
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/presentation_row_title_tv")));
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/presentation_book_iv")));
		WebElement View_Book_detail = driver.findElement(By.id("com.txtr.android.mmm:id/presentation_book_iv"));
		View_Book_detail.click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/document_details_buttons_books_of_interest")));
		WebElement Books_of_interest = driver.findElement(By.id("com.txtr.android.mmm:id/document_details_buttons_books_of_interest"));
		WebElement Books_of_interest_name = driver.findElement(By.id("com.txtr.android.mmm:id/mmmchannelsactivity_newlyarriveditemtitle"));
		String Books_of_interest_names = Books_of_interest_name.getText();
		
		//Flagging the book of interest
		Books_of_interest.click();
		
		//Navigating the left menu
		left_nav.click();
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/leftnav_mybooks")));
		WebElement mybooks_title= driver.findElement(By.id("com.txtr.android.mmm:id/leftnav_mybooks"));
		mybooks_title.click();
		
		//Clicking on the Flag of interest
		wait.until(ExpectedConditions.presenceOfElementLocated(By.className("android.widget.ImageButton")));
		WebElement books_of_interest_icon = driver.findElement(By.className("android.widget.ImageButton"));
		books_of_interest_icon.click();
		
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/mmmchannelsactivity_newlyarriveditemtitle")));
		WebElement My_Books_Books_of_interest = driver.findElement(By.id("com.txtr.android.mmm:id/mmmchannelsactivity_newlyarriveditemtitle"));
		String My_Books_Books_of_interest_name = My_Books_Books_of_interest.getText();
		System.out.println(My_Books_Books_of_interest_name);
		System.out.println(My_Books_Books_of_interest);
		System.out.println(Books_of_interest_name);
		assert My_Books_Books_of_interest_name.equals(Books_of_interest_names);
		System.out.println("Assert Failed");
		My_Books_Books_of_interest.click();
		books_of_interest_icon.click();
		
		
		
			
	}
	public void setAuthorInfoForReports() {
        ATUReports.setAuthorInfo("Sundara Subramanian", Utils.getCurrentTime(),"1.0");
 } 
	
	 public void setIndexPageDescription() {
        ATUReports.indexPageDescription = "Bibliotheca Cloud Library Client Apps<br/> <b> Android Application Testing </b>";
 }
}
