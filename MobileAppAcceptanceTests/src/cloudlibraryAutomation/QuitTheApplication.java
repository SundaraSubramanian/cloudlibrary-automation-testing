package cloudlibraryAutomation;
import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class QuitTheApplication extends LogOutoftheApplication {
	
@AfterClass	
public void QuitApplication()  throws Exception{
					driver.quit();
			
		
		}
public void setAuthorInfoForReports() {
    ATUReports.setAuthorInfo("Sundara Subramanian =", Utils.getCurrentTime(),"1.0");
}

 public void setIndexPageDescription() {
    ATUReports.indexPageDescription = "Bibliotheca Cloud Library Client Apps --> Android Application Testing";
}

@SuppressWarnings("deprecation")
public void testME() {
    setAuthorInfoForReports();
    ATUReports.add("Step Desc", false);
    ATUReports.add("Step Desc", "inputValue", false);
    ATUReports.add("Step Desc", "expectedValue", "actualValue", false);
    ATUReports.add("Step Desc", "inputValue", "expectedValue","actualValue", false);
}

public void testNewLogs() throws AWTException, IOException {

    ATUReports.add("INfo Step", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
    ATUReports.add("Pass Step", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
    WebElement element = driver.findElement(By.xpath("/html/body/div/h1/a"));
    ATUReports.add("Warning Step", LogAs.WARNING,new CaptureScreen(element));
    ATUReports.add("Fail step", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
}
	}


