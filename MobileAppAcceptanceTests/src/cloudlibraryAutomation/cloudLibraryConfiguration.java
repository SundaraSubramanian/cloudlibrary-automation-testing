package cloudlibraryAutomation;
import io.appium.java_client.*; 
import atu.testng.reports.ATUReports;
import atu.testng.reports.listeners.ATUReportsListener;
import atu.testng.reports.listeners.ConfigurationListener;
import atu.testng.reports.listeners.MethodListener;
import atu.testng.reports.utils.Utils;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.*;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.*;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.RemoteWebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.*;

@Listeners({ ATUReportsListener.class, ConfigurationListener.class,	  MethodListener.class })
@SuppressWarnings("unused")
public class cloudLibraryConfiguration {
	
	@SuppressWarnings("rawtypes")
	public static AndroidDriver driver;	
	String Test_Environment = "QA";
	String State = "District of Columbia";
	String User_Name= "Eddy05";
	String Library_Name="Central Library";
	String Search_Book_Name="Build a smokehouse";
	String first_featured_shelf_title= "Computer";
	@SuppressWarnings("rawtypes")
	@BeforeClass	
	
	public void Welcome() throws MalformedURLException, InterruptedException, ExecuteException, IOException{
		System.out.println("Welcome to Cloud Library test Automation");
		setIndexPageDescription();
		setAuthorInfoForReports();
		ATUReports.setWebDriver(driver);
		DesiredCapabilities capabilities = new DesiredCapabilities();
		DefaultExecutor executor = new DefaultExecutor();
	    DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();
		
		 /*CommandLine launchEmul = new CommandLine("C:/Program Files/Genymobile/Genymotion/player");
	        launchEmul.addArgument("--vm-name");
	        launchEmul.addArgument("\""+deviceName+"\"");
	        executor.setExitValue(1);
	        executor.execute(launchEmul, resultHandler);
	        Thread.sleep(40);*/

	    capabilities.setCapability("deviceName","192.168.56.101:5555");   
	    capabilities.setCapability("platformVersion", "6.0 ");
	    capabilities.setCapability("platformName", "Android");
		capabilities.setCapability("BROWSER_NAME", "Android");
		/*capabilities.setCapability("VERSION", " "); 
		capabilities.setCapability("platformName" , "Android");*/
		capabilities.setCapability("appPackage", "com.txtr.android.mmm");
		capabilities.setCapability("appActivity","com.txtr.android.mmm.ui.intro.MmmIntroActivity");
		//capabilities.setCapability("deviceName","04991ff6");
		driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
				
	}
	 public void setAuthorInfoForReports() {
         ATUReports.setAuthorInfo("Sundara Subramanian", Utils.getCurrentTime(),"1.0");
  } 
	
	 public void setIndexPageDescription() {
         ATUReports.indexPageDescription = "Bibliotheca Cloud Library Client Apps<br/> <b> Android Application Testing </b>";
  }

	 }


