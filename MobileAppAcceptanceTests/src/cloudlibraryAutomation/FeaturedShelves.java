package cloudlibraryAutomation;

import java.awt.AWTException;
import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;

public class FeaturedShelves extends InitialLeftNavigation {

	@Test (priority=4)	

	public void Featured_Shelves() throws Exception {
		
		WebElement featured_title= driver.findElement(By.id("com.txtr.android.mmm:id/leftnav_featured"));
		assert featured_title.getText().equals("Featured");
		featured_title.click();
		
				
		WebDriverWait wait = new WebDriverWait(driver, 80);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/presentation_row_title_tv")));
		WebElement featured_shelf_title= driver.findElement(By.id("com.txtr.android.mmm:id/presentation_row_title_tv"));
		assert featured_shelf_title.getText().equals(first_featured_shelf_title);
		
		System.out.println("Featured Shelf Name - Verification Successful");
				
	}
	public void setAuthorInfoForReports() {
        ATUReports.setAuthorInfo("Sundara Subramanian =", Utils.getCurrentTime(),"1.0");
 }
	
	 public void setIndexPageDescription() {
        ATUReports.indexPageDescription = "Bibliotheca Cloud Library Client Apps --> Android Application Testing";
 }
	
	
	 @SuppressWarnings("deprecation")
	public void testME() {
         setAuthorInfoForReports();
         ATUReports.add("Step Desc", false);
         ATUReports.add("Step Desc", "inputValue", false);
         ATUReports.add("Step Desc", "expectedValue", "actualValue", false);
         ATUReports.add("Step Desc", "inputValue", "expectedValue","actualValue", false);
  }
	 
	 public void testNewLogs() throws AWTException, IOException {

         ATUReports.add("INfo Step", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
         ATUReports.add("Pass Step", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
         WebElement element = driver.findElement(By.xpath("/html/body/div/h1/a"));
         ATUReports.add("Warning Step", LogAs.WARNING,new CaptureScreen(element));
         ATUReports.add("Fail step", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
  }
	 
	 

}
