package cloudlibraryAutomation;
import org.testng.annotations.Test;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.reports.utils.Utils;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import java.awt.AWTException;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;



public class LaunchtheApplication extends cloudLibraryConfiguration {

@Test (priority=1)
public void Launch_the_application() throws Exception{
	
	System.out.println("Launch the Cloud Library Application");
	
	//*(*(*(*(Select the Environment)*)*)*)*\\\
				WebDriverWait wait = new WebDriverWait(driver, 80);
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@text='"+Test_Environment+"']")));
				WebElement select_environment = driver.findElement(By.xpath("//android.widget.TextView[@text= '"+Test_Environment+"']"));
				select_environment.click();

	//*(*(*(*(Connect to the Library)*)*)*)*\\\
				WebElement connecttolibrary= driver.findElement(By.id("com.txtr.android.mmm:id/next_button"));
				connecttolibrary.click();
					
	//*(*(*(*(Waiting for the Country)*)*)*)*\\
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/spinner")));
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@index='9']")));	
				WebElement select_state = driver.findElement(By.xpath("//android.widget.TextView[@index='9']"));
				select_state.click();
				
	//*(*(*(*(Validate the Library)*)*)*)*\\
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.TextView[@index='0']")));
				WebElement select_library= driver.scrollTo(Library_Name);
				select_library.click();

	//*(*(*(*(Accept the terms and conditions)*)*)*)*\\
				wait.until(ExpectedConditions.presenceOfElementLocated(By.id("com.txtr.android.mmm:id/eula_accept_button")));
				wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.view.View[@content-desc='Privacy Policy']")));
				WebElement Accept_terms= driver.findElement(By.id("com.txtr.android.mmm:id/eula_accept_button"));
				Accept_terms.click();
				
			
		
	}

public void setAuthorInfoForReports() {
    ATUReports.setAuthorInfo("Sundara Subramanian =", Utils.getCurrentTime(),"1.0");
}

public void setIndexPageDescription() {
    ATUReports.indexPageDescription = "Bibliotheca Cloud Library Client Apps --> Android Application Testing";
}

@SuppressWarnings("deprecation")
public void testME() {
    setAuthorInfoForReports();
    ATUReports.add("Step Desc", false);
    ATUReports.add("Step Desc", "inputValue", false);
    ATUReports.add("Step Desc", "expectedValue", "actualValue", false);
    ATUReports.add("Step Desc", "inputValue", "expectedValue","actualValue", false);
}

public void testNewLogs() throws AWTException, IOException {

    ATUReports.add("INfo Step", LogAs.INFO, new CaptureScreen(ScreenshotOf.BROWSER_PAGE));
    ATUReports.add("Pass Step", LogAs.PASSED, new CaptureScreen(ScreenshotOf.DESKTOP));
    WebElement element = driver.findElement(By.xpath("/html/body/div/h1/a"));
    ATUReports.add("Warning Step", LogAs.WARNING,new CaptureScreen(element));
    ATUReports.add("Fail step", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
}
	
}
